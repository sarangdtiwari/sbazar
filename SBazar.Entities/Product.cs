﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBazar.Entities
{
    public class Product
    {
        public int ID { get; set; }
        public string  Name { get; set; }
        public string  Description { get; set; }
        public decimal Price { get; set; }
        public Category Category { get; set; }
            
    }
}
