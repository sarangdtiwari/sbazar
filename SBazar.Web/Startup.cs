﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SBazar.Web.Startup))]
namespace SBazar.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
